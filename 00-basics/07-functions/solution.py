#!/usr/bin/env python3

def my_func(*args,op):
	if op=="sum":
		return sum(args)
	elif op=="mul":
		r = 1
		for x in args:
			r *= x
		return r
	else:
		return "???"

my_list = [1,2,3,4,5]

for op in ["sum","mul"]:
	print(op,my_list,":",my_func(*my_list,op=op))

