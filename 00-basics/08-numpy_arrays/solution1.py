#!/usr/bin/env python3

import numpy as np

# dimensions
D = 4
# number of vectors
N = 3

# generate random unit complex vector and append it to the list
v = np.random.randn(D)+1j*np.random.randn(D)
v /= np.linalg.norm(v)
unit_vecs = [v]

for n in range(1,N):
	# generate new candidate
	v = np.random.randn(D)+1j*np.random.randn(D)
	for i in range(n):
		# subtract the projection onto earlier vectors
		v -= np.dot(unit_vecs[i].conj(),v)*unit_vecs[i]
	# normalize and append to the list
	v /= np.linalg.norm(v)
	unit_vecs.append(v)

# show the vectors
for v in unit_vecs:
	print(v)

# prove that the vectors are all orthogonal
proof = np.empty((N,N),dtype=unit_vecs[0].dtype)
for i in range(N):
	for j in range(N):
		u = unit_vecs[i].conj()
		v = unit_vecs[j]
		proof[j,i] = np.dot(u,v)
proof[np.abs(proof)<1e-15] = 0 # <-ignore numerical artifacts
print(proof.real)
print(proof.imag)

