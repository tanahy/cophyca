#!/usr/bin/env python3

import numpy as np
from matplotlib import pyplot as plt

data = np.loadtxt("task3.txt").T
print("data shape:",data.shape)

# covariance matrix
cov = np.cov(data)
w, v = np.linalg.eig(cov)

# generate sorting (highest to lowest) permutation determined by absolute value of eigenvalues
perm = np.argsort(-np.abs(w))
w = w[perm]
v = v.T[perm].T
print(w)
v_inv = np.linalg.inv(v)

# change the basis
data_red = np.dot(v_inv,data)

# show results
plt.scatter(data_red[0],data_red[1])

plt.show()

