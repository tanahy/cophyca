#!/usr/bin/env python3

import numpy as np
from matplotlib import pyplot as plt

x = np.linspace(-2,2,101)
dx = x[1]-x[0]
y = np.random.rand(len(x))

# get corresponding frequencies
f = np.fft.fftfreq(len(x),dx)

# filter
C = f.copy()
C[f==0] = 1.0
C = 1/C/C
dk = 1.0/dx
C /= np.sqrt(np.sum(C)*dk)
C *= 5.0
C[f==0] = 1.0

y_ft = np.fft.fft(y)
# apply filter
y_ft_filtered = C*y_ft
z = np.fft.ifft(y_ft_filtered)

fig, ax = plt.subplots(2)

# plot in x space
ax[0].plot(x,y)
ax[0].plot(x,z)

# plot in frequency space
ax[1].set_yscale("log")
ax[1].scatter(f,np.abs(y_ft))
ax[1].scatter(f,C)
ax[1].scatter(f,np.abs(y_ft_filtered))

plt.show()

