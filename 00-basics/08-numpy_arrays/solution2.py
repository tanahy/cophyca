#!/usr/bin/env python3

import numpy as np
from matplotlib import pyplot as plt

N = 10000

# 3 corners of the Sierpiński triangle
r = 0.5
p = np.array([-2*np.pi/3,0,2.0*np.pi/3])
p = r*np.array([np.sin(p),np.cos(p)]).T

# initiate the list of points on the triangle with one random point
v = [p[np.random.randint(len(p))]]

# iterate
while len(v)<N:
	r = p[np.random.randint(len(p))]
	v.append((r+v[-1])/2)

v = np.array(v).T

# show results
plt.scatter(v[0],v[1])

plt.show()

