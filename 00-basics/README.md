# Language

This section of the course is a translation with small modifications on notes written by [Sergio Delgado Quintero](https://sdelquin.me/) for an [Introduction to Python](https://github.com/pythoncanarias/eoi/) course given by EOI and Python Canarias in 2020.

The goal of this module is to understand the fundamental concepts of the Python programming language. Learning data structures and control structure will allow you to use more advanced tools/libraries later on.

Order | Contents
--- | ---
0 | [Introduction](00-intro/00-intro.ipynb)
1 | [Data: types, values, variables and names](01-data/01-data.ipynb)
2 | [Numbers](02-numbers/02-numbers.ipynb)
3 | [Strings](03-strings/03-strings.ipynb)
4 | [Condicionals](04-conditionals/04-conditionals.ipynb)
5 | [Loops](05-loops/05-loops.ipynb)
6 | [Lists y Tuples](06-lists/06-lists.ipynb)
7 | [Dictionaries](07-dicts/07-dicts.ipynb)
8 | [Funcions](08-functions/08-functions.ipynb)
9 | [Objetcs and Classes](09-objects/09-objects.ipynb)
10 | [Files, modules and packages](10-files/10-files.ipynb)
