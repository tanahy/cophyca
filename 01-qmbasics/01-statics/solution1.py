#!/usr/bin/env python3

import numpy as np
from matplotlib import pyplot as plt

L = 3.0
N = 256
X = np.linspace(-L/2,L/2,N,endpoint=False)
dX = L/N

def norm(phi):
	return np.sqrt(np.sum(np.abs(phi)**2)*dX)

laplacian = np.array([1,-2,1])/(dX**2)
H = np.empty((N,N))
for i in range(N):
	l = (i-1+N)%N
	r = (i+1)%N
	H[i,l] = -0.5*laplacian[0]
	H[i,i] = -0.5*laplacian[1]
	H[i,r] = -0.5*laplacian[2]


H[0,N-1] = 0
H[N-1,0] = 0

eigvals, eigvecs = np.linalg.eigh(H)
eigvecs = eigvecs.T

for i in range(len(eigvecs)):
	eigvecs[i] /= norm(eigvecs[i])

fig, ax = plt.subplots(1,2)

n = np.arange(51)
ax[0].scatter(n,0.5*np.pi*np.pi*n*n/L/L,marker="o",color="grey")
ax[0].scatter(n,eigvals[:len(n)],marker=".")

ax[1].plot(X,eigvecs[0])
ax[1].plot(X,eigvecs[1])
ax[1].plot(X,eigvecs[2])

plt.show()

