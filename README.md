# Lectures on Computational Physics of Cold Atoms

### Authors: T. Hernández, M. Kruk, E. Witkowska

Series of lectures given in a hands-on style at the Institute of Physics of the Polish Academy of Sciences (IFPAN) as an introductory course on computational physics applied to selected topics on the field of cold atoms and quantum gases. The lectures will be given in person starting October 2022 at IFPAN, Warsaw, but anyone can replicate them by following the provided Jupyter Notebooks in this repository. 

The format of the lectures will be mainly practical by proposing different simplified physical problems related to the research on cold atoms and solving them numerically using basic computational building blocks. After the students are familiar with the methods used to build the tools needed to solve each problem, examples of already made libraries to tackle said problem and further extensions might be shown. The chosen programming language for the course will be Python.

## Requirements

The lectures can be followed by simply reading the notebooks provided, although we highly encourage participation in the lectures by programming your own proposals and checking the proposed solutions only afterwards. 

Students should have basic progamming knowledge on any language, a brief introduction to Python will be given if requested by a sufficient amount of participants. Computational power is not needed as the examples used will have low requirements in terms of hardware. 

If you decide to code during the lectures, you will require at least the following in your installation:

- Python 3
```https://realpython.com/installing-python/```
	- Numpy (library)
	```pip install numpy```
	- Matplotlib (library)
	```pip install matplotlib```
- Jupyter Notebook
```pip install jupyter```
- This repository 
```git clone https://gitlab.com/tanahy/cophyca```


## Syllabus

The lectures will be split into four blocks of content of 3 to 4 hours each, amounting to a total of 15 hours.

- #### Preparation of Basic Tools (3h)
- #### General Methods for Quantum Gases (4h)
	- Dynamical evo lution with the Schrodinger Equation
	- Stochastic Methods at non-zero temperature
	- Bose gases
	- Monte Carlo Methods
	- Hydrodynamic equations (TBD)
- #### Atoms in Optical Lattices (4h)
	- Hubbard model 
	- Eigenstates and construction of Fock basis
 	- Unitary dynamics
 	- Non-zero temperature and open quantum systems (TBD)
 - #### Spin models (4h)
	- Construction of pseudo-spin operators 
	- Total spin states
	- Heisenberg like models
	- Collective spin models
